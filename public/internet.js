export class Internet
{
  /**
   * @callback Internet~getJSONCallback
   * @param {?Object} err An error object
   * @param {?Object} data The data
   * @param {XMLHttpRequest} xhr The request
   */
  /**
   * 
   * @param {string} url 
   * @param {Internet~getJSONCallback} callback 
   */
  static getJSON(url, callback)
  {
    let xhr = new XMLHttpRequest();

    xhr.onload = (req, ev) =>
    {
      try
      {
        let json = JSON.parse(xhr.responseText);
        callback(undefined, json, xhr);
      }
      catch (error)
      {
        callback({ error, source: xhr.responseText }, undefined, xhr);
      }
    };

    xhr.onerror = (req, ev) =>
    {
      callback(ev, undefined, xhr);
    };

    xhr.open('GET', url);
    xhr.send();
  }
}