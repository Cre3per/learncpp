class Main
{
  /**
   * 
   */
  constructor()
  {
    for (let chapter of CONFIG.chapters)
    {
      let table = document.createElement('table');
      table.classList.add('chapter');

      let thead = document.createElement('thead');
      let tbody = document.createElement('tbody');

      let th = document.createElement('th');

      th.innerText = chapter.chapter;

      thead.appendChild(th);

      for (let lesson of chapter.lessons)
      {
        let tr = document.createElement('tr');
        
        let tdLesson = document.createElement('td');
        tdLesson.innerText = lesson.lesson;

        tr.appendChild(tdLesson);

        let tdStds = document.createElement('td');
        let tableStd = document.createElement('table');
        let tbodyStd = document.createElement('tbody');

        for (let std of lesson.std)
        {
          let trStd = document.createElement('tr');
          trStd.classList.add('std');

          let a = document.createElement('a');
          
          a.href = std.href;
          a.innerText = std.name;

          let tdStdHref = document.createElement('td');
          tdStdHref.appendChild(a);

          trStd.appendChild(tdStdHref);

          let span = document.createElement('span');
          
          span.innerText = std.desc;

          let tdStdDesc = document.createElement('td');
          tdStdDesc.appendChild(span);

          trStd.appendChild(tdStdDesc);

          tbodyStd.appendChild(trStd);
        }

        tableStd.appendChild(tbodyStd);
        tdStds.appendChild(tableStd);

        tr.appendChild(tdStds);

        tbody.appendChild(tr);
      }

      table.appendChild(thead);
      table.appendChild(tbody);

      document.body.appendChild(table);
    }

  }
}

let oldLoad = window.onload;
window.onload = () =>
{
  oldLoad && oldLoad();

  new Main();
};