const CONFIG = {
  chapters: [
    {
      chapter: '6',
      lessons: [
        {
          lesson: '6.3',
          std: [
            {
              name: 'std::accumulate',
              href: 'http://www.cplusplus.com/reference/numeric/accumulate/',
              desc: 'Sum up arrays'
            },
            {
              name: 'std::accumulate',
              href: 'http://www.cplusplus.com/reference/numeric/accumulate/',
              desc: 'Sum up arrays'
            },
          ]
        },
      ]
    },
  ]
};