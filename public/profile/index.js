import { Internet } from '../internet.js';

class IdentificationMethod
{
  static get USERNAME()
  {
    return 0;
  }

  static get EMAIL()
  {
    return 1;
  }
}

class Main
{
  /**
   * @param {Object} comment The comment to check
   * @return {boolean} True if the given comment was posted by the user
   */
  _isUserComment(comment)
  {
    if (this.identificationMethod === IdentificationMethod.USERNAME)
    {
      return (comment.author_name === config.user.name);
    }
    else if (this.identificationMethod === IdentificationMethod.EMAIL)
    {
      return comment.author_avatar_urls['24'].includes(config.user.mailmd5);
    }


    console.error(`Invalid identification method: ${this._identificationMethod}`);

    return false;
  }

  /**
   * Formats a duration to a friendly string
   * @param {number} duration
   * @return {string} The formatted duration
   */
  _formatDuration(duration)
  {
    const ORDERS_OF_MAGNITUDE = [ 'k', 'M', 'G', 'T' ];

    for (let i = (ORDERS_OF_MAGNITUDE.length - 1); i >=0; --i)
    {
      let scale = Math.pow(10, (i + 1) * 3);

      if (duration >= scale)
      {
        return (parseInt(duration / scale) + ORDERS_OF_MAGNITUDE[i] + 's');
      }
    }

    return (parseInt(duration) + 's');
  }

  /**
   * @callback Main~getRatioCallback
   * @param {?object} err
   * @param {?number} ratio Percentage of comments which were actually made by
   *                        the user.
   */
  /**
   * Calculates the ratio of posts by the user to posts including the user's
   * name.
   * Shouldn't be called on the client side.
   * @param {Main~getRatioCallback} callback
   */
  _getRatio(callback)
  {
    let totalPages = 0;
    let totalComments = 0;

    let mentions = 0;

    let processPage = (pageNumber) =>
    {
      const PAGE_SIZE = 100;

      Internet.getJSON(`https://www.learncpp.com/wp-json/wp/v2/comments?per_page=${PAGE_SIZE}&page=${pageNumber}&search=${config.user.name}`, (err, data, req) =>
      {
        console.log(`Processing page ${pageNumber}/${totalPages}`);

        if (err)
        {
          callback(err);
        }
        else
        {
          if (totalPages === 0)
          {
            totalPages = parseInt(req.getResponseHeader('X-WP-TotalPages'));
            totalComments = parseInt(req.getResponseHeader('X-WP-Total'));
          }

          for (let comment of data)
          {
            if (!this._isUserComment(comment))
            {
              ++mentions;
            }
          }

          if (pageNumber === totalPages)
          {
            callback(undefined, (totalComments - mentions) / totalComments);
          }
          else
          {
            processPage(pageNumber + 1);
          }
        }
      });
    };

    processPage(1);
  }

  /**
   * Loads stats and updates the view
   */
  _loadStats()
  {
    const PAGE_SIZE = 10;

    Internet.getJSON(`https://www.learncpp.com/wp-json/wp/v2/comments?per_page=${PAGE_SIZE}&search=${config.user.name}`, (err, data, req) =>
    {
      if (err)
      {
        console.error(err);
      }
      else
      {
        let totalComments = parseInt(parseFloat(req.getResponseHeader('X-WP-Total')) * config.ownCommentPerc);
        let lastActive = 0;

        for (let comment of data)
        {
          if (this._isUserComment(comment))
          {
            lastActive = Date.parse(`${comment.date_gmt}+00:00`);
            break;
          }
        }

        document.querySelector('#comments_total').innerText = totalComments.toString();
        document.querySelector('#comments_total').classList.remove('loading');

        let secondsSinceLastPost = (((new Date()).getTime() - lastActive) / 1000);

        // In case the server is ahead of time
        secondsSinceLastPost = Math.max(0, secondsSinceLastPost);

        document.querySelector('#last_active').innerText = `${this._formatDuration(secondsSinceLastPost)} ago`;
        document.querySelector('#last_active').classList.remove('loading');
      }
    });
  }

  /**
   * Populates the view with config data
   */
  _populate()
  {
    document.title = config.user.name;

    document.querySelector('#name').innerText = config.user.name;
    document.querySelector('#avatar').src = `https://www.gravatar.com/avatar/${config.user.mailmd5}?s=${config.avatarSourceScale}`;
  }

  /**
   * 
   */
  constructor()
  {
    /**
     * @type {IdentificationMethod}
     */
    this.identificationMethod = IdentificationMethod.EMAIL;

    this._populate();

    this._loadStats();

    // Only run this to set up config.ownCommentPerc for a new user
    // this._getRatio((err, ratio) =>
    // {
    //   err && console.error(err);
    //   console.log('Percentage of own comments: ' + ratio);
    // });

    
  }
}

let oldLoad = window.onload;
window.onload = () =>
{
  oldLoad && oldLoad();

  new Main();
};