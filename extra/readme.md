# Inspired by and based on, but not affiliated with [learncpp.com](https://learncpp.com).

---

This repository aims to provide updates to the content of
[learncpp.com](https://learncpp.com).  
Lessons might make use of features that have been announced to be part of C++ in
a future version, but are not yet widely supported by compilers. All lessons
discussing these features might be untested and could ultimately change. All
such content is marked as `C++20`. If you want to reproduce the code shown in
these lessons you need to enable the highest possible standard support in your
compiler's options. Even then, the features might not be available yet.

Chapters and lessons marked as _WIP_ are Work In Progress. Their content might
be incomplete or wrong.

# Updates

## Chapter 4 - Variable Scope and More Types

7. [Structs](4/7.md)

## Chapter 5 - Relational operators and floating point comparisons

6. [Relational operators and floating point comparisons](5/6.md)

## Chapter 6 - Arrays, Strings, Pointers, and References

12a. [For each loops](7/12a.md)

## Chapter 7 - Functions

7. [Default parameters](7/7.md)

## Chapter 13 - Templates

1. [Function templates](13/1.md)

## Chapter 14 - Exceptions

8. [Exception dangers and downsides](14/8.md)

---

# Extras

## Chapter 7

15 - [Lambda functions](7/15.md)

## Threads (WIP)
1. [Introduction to threads](Threads/Introduction%20to%20threads/Introduction%20to%20threads.md)
2. [Returning values from threads](Threads/Returning%20values%20from%20threads/Returning%20values%20from%20threads.md)
3. [Synchronization](Threads/Synchronization/Synchronization.md)
4. [Execution policies](Threads/Execution%20policies.md)