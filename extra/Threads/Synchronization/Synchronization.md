In [Introduction to threads](../Introduction%20to%20threads/Introduction%20to%20threads.md) you saw that accessing the same object simultaneously can be problematic.  
A more severe example, one that could cause a crash is the following

[//]:(#snippet1)
```cpp
#include <cstring>
#include <iostream>
#include <thread>
#include <vector>

class CPlayer
{
private:
  char *m_szName{ nullptr };

public:
  const char *getName(void) const
  {
    return this->m_szName;
  }

  void setName(const char *szName)
  {
    delete[] this->m_szName;

    if (szName == nullptr)
    {
      this->m_szName = nullptr;
    }
    else
    {
      this->m_szName = new char[std::strlen(szName) + 1];
      std::strcpy(this->m_szName, szName);
    }
  }
};

void printPlayer(const CPlayer &player)
{
  while (true)
  {
    if (player.getName() != nullptr)
    {
      for (int i{ 0 }; i < 5; ++i)
      {
        std::cout << player.getName()[i];
      }

      std::cout << std::endl;
    }
  }
}

void updatePlayerName(CPlayer &player)
{
  constexpr const char *k_arrNames[]{ "Peter", "Alexander", "Thomas", nullptr };
  constexpr int k_iNames{ sizeof(k_arrNames) / sizeof(k_arrNames[0]) };

  int iName{ 0 };

  while (true)
  {
    player.setName(k_arrNames[iName++]);

    if (iName == k_iNames)
    {
      iName = 0;
    }
  }
}

int main(void)
{
  CPlayer player{};

  std::thread thrPrint{ printPlayer, std::cref(player) };
  std::thread thrUpdate{ updatePlayerName, std::ref(player) };

  thrPrint.join();
  thrUpdate.join();

  return 0;
}
```

`printPlayer` could be a rendering function of a game. It prints the first 5 characters of the player's name, assuming that all names are at least 5 characters long.  
`updatePlayerName` could be a function that updates the username with data received from Steam for example. Sometimes a networking error occurs and the player's name is set to a `nullptr`, but `printPlayer` checks for `nullptr` before accessing the name, so this isn't a problem, right?  
If the program ran sequentially, always updating the name before printing it, there wouldn't be a problem. But now both functions run at the same time. What can happen is this

1. The player's name is valid
2. `printPlayer` executes `if (player.getName() != nullptr)`, which succeeds
3. `updatePlayerName` sets the name to a `nullptr`
4. `printPlayer` enters the loop, trying to access a `nullptr`
5. Crash

The risk of this happening is present every time you're writing to a variable shared between threads. The only safe access is reading. We can have multiple threads all reading at the same time without problems. But as soon as one thread writes to the variable, there is a chance of something getting out of hand.

### `std::mutex`, `std::lock_guard` and `std::unique_lock`

The most basic way of preventing this are mutual exclusions. They work by halting all threads that want to access a variable while this variable is in use by another thread.

[//]:(#snippet3)

```cpp
#include <mutex> // For std::mutex and std::lock_guard

// Create a mutex that will be used to lock access to the player's name.
std::mutex g_mutPlayerName{ };

void printPlayer(const CPlayer &player)
{
  while (true)
  {
    // Create a lock on the mutex
    std::lock_guard<std::mutex> lgPlayerName{ g_mutPlayerName };
    
    if (player.getName() != nullptr)
    {
      for (int i{ 0 }; i < 5; ++i)
      {
        std::cout << player.getName()[i];
      }

      std::cout << std::endl;
    }
  }
}

void updatePlayerName(CPlayer &player)
{
  constexpr const char *k_arrNames[]{ "Peter", "Alexander", "Thomas", nullptr };
  constexpr int k_iNames{ sizeof(k_arrNames) / sizeof(k_arrNames[0]) };

  int iName{ 0 };

  while (true)
  {
    // Create a lock on the mutex
    std::lock_guard<std::mutex> lgPlayerName{ g_mutPlayerName };

    player.setName(k_arrNames[iName++]);

    if (iName == k_iNames)
    {
      iName = 0;
    }
  }
}
```

A specific variable cannot be locked directly, so a new variable `g_mutPlayerName` of type `std::mutex` is created. An `std::mutex` can be locked by creating an `std::lock_guard` and passing the mutex to it's constructor.  
When a lock guard is constructed, the mutex is considered to be _owned_ by this lock guard. If a lock guard attempts to take ownership of a mutex that is already owned by another lock guard, the thread will be suspended until the mutex no longer has an owner. Ownership is released on destruction of the owning lock guard. In this example, ownership is released at the end of each `while`-loop.  
As long as the mutex is locked by one thread, the other thread is prevented from running, slowing it down. This would pose a major performance degradation, unacceptable for eg. video games.  
We don't have all too much code after the last use of the player's name, still, the reset of `iName` in `updatePlayerName` and the line feed in `printPlayer` don't need to be protected by the mutex.  
`std::lock_guard` has no ability to manually unlock the mutex, it can only do so on destruction. Manually calling `std::lock_guard`'s destructor might seem like a viable options, but you'll find that this doesn't work, because the automatic destruction of the lock guard will try to unlock the mutex again, resulting in undefined behavior.  
`std::lock_guard` isn't the only locking wrapper for mutexes. There's also `std::unique_lock`, which offers manual unlocking and locking features for repeated use of the same lock. As with `std::lock_guard`, construction tries to take ownership of the mutex and suspends the thread until it can do so. Destruction releases the mutex if the mutex is locked.

[//]:(#snippet4)

```cpp
#include <mutex> // For std::unique_lock

std::mutex g_mutPlayerName{ };

void printPlayer(const CPlayer &player)
{
  while (true)
  {
    // The lock_guard is now a unique_lock
    std::unique_lock<std::mutex> lockPlayerName{ g_mutPlayerName };
    
    if (player.getName() != nullptr)
    {
      for (int i{ 0 }; i < 5; ++i)
      {
        std::cout << player.getName()[i];
      }

      // We don't access the player anymore, release the mutex.
      lockPlayerName.unlock();

      std::cout << std::endl;
    }
  }
}

void updatePlayerName(CPlayer &player)
{
  constexpr const char *k_arrNames[]{ "Peter", "Alexander", "Thomas", nullptr };
  constexpr int k_iNames{ sizeof(k_arrNames) / sizeof(k_arrNames[0]) };

  int iName{ 0 };

  while (true)
  {
    // The lock_guard is now a unique_lock
    std::unique_lock<std::mutex> lockPlayerName{ g_mutPlayerName };

    player.setName(k_arrNames[iName++]);

    // We don't access the player anymore, release the mutex.
    lockPlayerName.unlock();

    if (iName == k_iNames)
    {
      iName = 0;
    }
  }
}
```

Note that the mutex itself has `lock` and `unlock` member functions. These should not be used unless you absolutely know what you're doing and don't want the overhead of using the wrapper classes. If an exception is thrown after the mutex was locked and before it is unlocked, the mutex will remain in a locked state. The wrapper classes `std::lock_guard` and `std::unique_lock` don't have this problem, because they unlock the mutex on destruction.