#include <mutex> // For std::unique_lock

std::mutex g_mutPlayerName{ };

void printPlayer(const CPlayer &player)
{
  while (true)
  {
    // The lock_guard is now a unique_lock
    std::unique_lock<std::mutex> lockPlayerName{ g_mutPlayerName };
    
    if (player.getName() != nullptr)
    {
      for (int i{ 0 }; i < 5; ++i)
      {
        std::cout << player.getName()[i];
      }

      // We don't access the player anymore, release the mutex.
      lockPlayerName.unlock();

      std::cout << std::endl;
    }
  }
}

void updatePlayerName(CPlayer &player)
{
  constexpr const char *k_arrNames[]{ "Peter", "Alexander", "Thomas", nullptr };
  constexpr int k_iNames{ sizeof(k_arrNames) / sizeof(k_arrNames[0]) };

  int iName{ 0 };

  while (true)
  {
    // The lock_guard is now a unique_lock
    std::unique_lock<std::mutex> lockPlayerName{ g_mutPlayerName };

    player.setName(k_arrNames[iName++]);

    // We don't access the player anymore, release the mutex.
    lockPlayerName.unlock();

    if (iName == k_iNames)
    {
      iName = 0;
    }
  }
}