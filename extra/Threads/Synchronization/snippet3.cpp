#include <mutex> // For std::mutex and std::lock_guard

// Create a mutex that will be used to lock access to the player's name.
std::mutex g_mutPlayerName{ };

void printPlayer(const CPlayer &player)
{
  while (true)
  {
    // Create a lock on the mutex
    std::lock_guard<std::mutex> lgPlayerName{ g_mutPlayerName };
    
    if (player.getName() != nullptr)
    {
      for (int i{ 0 }; i < 5; ++i)
      {
        std::cout << player.getName()[i];
      }

      std::cout << std::endl;
    }
  }
}

void updatePlayerName(CPlayer &player)
{
  constexpr const char *k_arrNames[]{ "Peter", "Alexander", "Thomas", nullptr };
  constexpr int k_iNames{ sizeof(k_arrNames) / sizeof(k_arrNames[0]) };

  int iName{ 0 };

  while (true)
  {
    // Create a lock on the mutex
    std::lock_guard<std::mutex> lgPlayerName{ g_mutPlayerName };

    player.setName(k_arrNames[iName++]);

    if (iName == k_iNames)
    {
      iName = 0;
    }
  }
}