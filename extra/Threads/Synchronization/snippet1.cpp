#include <cstring>
#include <iostream>
#include <thread>
#include <vector>

class CPlayer
{
private:
  char *m_szName{ nullptr };

public:
  const char *getName(void) const
  {
    return this->m_szName;
  }

  void setName(const char *szName)
  {
    delete[] this->m_szName;

    if (szName == nullptr)
    {
      this->m_szName = nullptr;
    }
    else
    {
      this->m_szName = new char[std::strlen(szName) + 1];
      std::strcpy(this->m_szName, szName);
    }
  }
};

void printPlayer(const CPlayer &player)
{
  while (true)
  {
    if (player.getName() != nullptr)
    {
      for (int i{ 0 }; i < 5; ++i)
      {
        std::cout << player.getName()[i];
      }

      std::cout << std::endl;
    }
  }
}

void updatePlayerName(CPlayer &player)
{
  constexpr const char *k_arrNames[]{ "Peter", "Alexander", "Thomas", nullptr };
  constexpr int k_iNames{ sizeof(k_arrNames) / sizeof(k_arrNames[0]) };

  int iName{ 0 };

  while (true)
  {
    player.setName(k_arrNames[iName++]);

    if (iName == k_iNames)
    {
      iName = 0;
    }
  }
}

int main(void)
{
  CPlayer player{};

  std::thread thrPrint{ printPlayer, std::cref(player) };
  std::thread thrUpdate{ updatePlayerName, std::ref(player) };

  thrPrint.join();
  thrUpdate.join();

  return 0;
}