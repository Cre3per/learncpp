#include <chrono>
#include <iostream>

class CReturningThread
{
private:
public:
};

int myFunction(void)
{
  // Use std::endl to flush the output buffer. Using '\n' could hold back the
  // output and make you think this line isn't reached until after the sleep.
  std::cout << "myFunction is running" << std::endl;

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  std::cout << "myFunction is done" << std::endl;

  return 9;
}

int main(void)
{
  // Create a CReturningThread and pass it the function we want to run in a
  // thread.
  CReturningThread thr{ myFunction };

  // myFunction is running

  // Wait for myFunction to finish and print it's return value.
  std::cout << thr.get() << '\n';

  return 0;
}