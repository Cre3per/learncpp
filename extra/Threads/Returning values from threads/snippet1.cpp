#include <chrono>
#include <iostream>
#include <thread>

void function1(int *pResult)
{
  // Sleep for 500 milliseconds, other threads keep on running
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  *pResult = 9;
}

void function2(int &iResult)
{
  // Sleep for 750 milliseconds, other threads keep on running
  std::this_thread::sleep_for(std::chrono::milliseconds(750));
  iResult = 12;
}

int main(void)
{
  int iResult1{ 0 };
  int iResult2{ 0 };

  std::thread thr1{ function1, &iResult1 };
  
  // This doesn't work!
  std::thread thr2{ function2, iResult2 };

  thr1.join();
  thr2.join();

  std::cout << iResult1 << '\n';
  std::cout << iResult2 << '\n';

  return 0;
}