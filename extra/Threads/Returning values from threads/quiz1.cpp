#include <chrono>
#include <functional>
#include <iostream>
#include <thread>

class CReturningThread
{
private:

  // The function supplied by the user
  std::function<int(void)> m_fn{ nullptr };

  // The thread we're going to run m_fn in
  std::thread m_thr{};

  // The result of m_fn
  int m_iResult{ 0 };

private:

  // Runs m_fn and saves it's return value to m_iResult
  void runInternal(void)
  {
    this->m_iResult = this->m_fn();
  }

public:

  // If m_thr is still running, waits for it to finish.
  // Returns the return value of m_fn
  int get(void)
  {
    this->m_thr.join();

    return this->m_iResult;
  }

  // Immediately starts running fn in a new thread
  CReturningThread(const std::function<int(void)> &fn)
      : m_fn{ fn }
  {
    this->m_thr = std::thread{ &CReturningThread::runInternal, this };
  }
};

int myFunction(void)
{
  // Use std::endl to flush the output buffer. Using '\n' could hold back the
  // output and make you think this line isn't reached until after the sleep.
  std::cout << "myFunction is running" << std::endl;

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  
  std::cout << "myFunction is done" << std::endl;

  return 9;
}

int main(void)
{
  CReturningThread thr{ myFunction };

  std::cout << thr.get() << '\n';

  return 0;
}