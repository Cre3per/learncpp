#include <thread>
#include <iostream>

void fn(const int *p)
{
  std::cout << *p;
}

int main(void)
{
  int i{ 0 };
  const int *p{ &i };

  std::thread thr{ fn, p };

  thr.join();

  return 0;
}