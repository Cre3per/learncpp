#include <chrono>
#include <future> // For std::async and std::future
#include <iostream>

int maxInt(int a, int b)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  return ((a > b) ? a : b);
}

int main(void)
{
  // Creates a new std::thread behind the scenes calling
  // maxInt(12, 50)
  std::future<int> fResult{ std::async(std::launch::async, maxInt, 12, 50) };

  // maxInt is running or already finished

  std::cout << fResult.get() << '\n';

  return 0;
}