#include <thread>

int myFunction(void)
{
  return 4;
}

int main(void)
{
  // This is allowed
  std::thread thr{ myFunction };

  thr.join();

  // But can't access the return value of myFunction

  return 0;
}