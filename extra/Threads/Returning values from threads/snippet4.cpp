#include <iostream>
#include <thread>
#include <functional> // For std::reference_wrapper

void myFunction(int &iResult)
{
  iResult = 12;
}

int main(void)
{
  int iResult{ 0 };

  std::thread thr{ myFunction, std::ref(iResult) };

  thr.join();

  std::cout << iResult << '\n';

  return 0;
}