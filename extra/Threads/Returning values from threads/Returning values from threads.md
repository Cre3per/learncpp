## Required lessons
- [Lesson 12.8 -- Object slicing](https://www.learncpp.com/cpp-tutorial/12-8-object-slicing/)

You have been using return values all throughout the tutorials, and reasonably so. When creating an `std::thread` there is no function call as we're used to, so we have to look for other ways of returning values.

### Returning from a thread function

Despite being able to pass a function with a return value to `std::thread::thread`, there is no way of accessing the return value.

[//]:(#snippet2)

```cpp
#include <thread>

int myFunction(void)
{
  return 4;
}

int main(void)
{
  // This is allowed
  std::thread thr{ myFunction };

  thr.join();

  // But can't access the return value of myFunction

  return 0;
}
```

### Pointers and references

Since we can pass arguments to threads we can also pass pointers which can be used by the thread to _return_ a value to the caller.

[//]:(#snippet1)

```cpp
#include <chrono>
#include <iostream>
#include <thread>

void function1(int *pResult)
{
  // Sleep for 500 milliseconds, other threads keep on running
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  *pResult = 9;
}

void function2(int &iResult)
{
  // Sleep for 750 milliseconds, other threads keep on running
  std::this_thread::sleep_for(std::chrono::milliseconds(750));
  iResult = 12;
}

int main(void)
{
  int iResult1{ 0 };
  int iResult2{ 0 };

  std::thread thr1{ function1, &iResult1 };
  
  // This doesn't work!
  std::thread thr2{ function2, iResult2 };

  thr1.join();
  thr2.join();

  std::cout << iResult1 << '\n';
  std::cout << iResult2 << '\n';

  return 0;
}
```

`function` and `thr1` work as expected. The function sleeps, then sets `*pResult` to `9` and finishes. `iResult` is `9` once `thr1.join` finishes.  
Unfortunately, passing references does not work, because thread arguments are passed as decay copies, ie. const, volatile and reference modifiers are removed from the argument. To get around this limitation we can use `std::reference_wrapper`, or it's wrapper `std::ref`.

[//]:(#snippet4)

```cpp
#include <iostream>
#include <thread>
#include <functional> // For std::ref

void myFunction(int &iResult)
{
  iResult = 12;
}

int main(void)
{
  int iResult{ 0 };

  std::thread thr{ myFunction, std::ref(iResult) };

  thr.join();

  std::cout << iResult << '\n';

  return 0;
}
```
Prints
```
12
```

Remember to watch out for dangling pointers and references. Your thread must finish before the referenced or pointed-to object gets destructed.

### Threading without `std::thread`

There are cases were you don't want to, or can't, a function. For example when the function is part of a library and you don't have access to the source code.  
Luckily there is a way to launch new threads and get the actual return value of a function.

[//]:(#snippet5)

```cpp
#include <chrono>
#include <future> // For std::async and std::future
#include <iostream>

int maxInt(int a, int b)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  return ((a > b) ? a : b);
}

int main(void)
{
  // Creates a new std::thread behind the scenes calling
  // maxInt(12, 50)
  std::future<int> fResult{ std::async(std::launch::async, maxInt, 12, 50) };

  // maxInt is running or already finished

  std::cout << fResult.get() << '\n';

  return 0;
}
```

`maxInt` behaves like `std::max` (plus the sleep) but is easier to work with, because `std::max` is templated.  
Instead of manually constructing an `std::thread` the function `std::async` can be called with `std::launch::async` is it's first parameter. `std::launch::async` tells `std::async` to create a new thread that runs the given function. The remaining parameters of `std::async` are equivalent to those of `std::thread` (The function we want to run followed by it's arguments).  
`std::future` is a class that provides easy and safe access to asynchronous operations. Calling `fResult.get` waits for `maxInt` to finish, if it hasn't finished already, and returns the return value of `maxInt`.


### Quiz

#### Quiz 1

Using `std::thread`, write a class called `CReturningThread` that accepts an int-returning function without parameters in it's constructor. Upon construction, a thread should be started. Calling `.get()` on the classes object should wait for the function to finish and return the function's return value.  
Do not use `std::async` or `std::future`.  
The following code should print
```
9
```

[//]:(#quiz1task)

```cpp
#include <chrono>
#include <iostream>

class CReturningThread
{
private:
public:
};

int myFunction(void)
{
  // Use std::endl to flush the output buffer. Using '\n' could hold back the
  // output and make you think this line isn't reached until after the sleep.
  std::cout << "myFunction is running" << std::endl;

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  std::cout << "myFunction is done" << std::endl;

  return 9;
}

int main(void)
{
  // Create a CReturningThread and pass it the function we want to run in a
  // thread.
  CReturningThread thr{ myFunction };

  // myFunction is running

  // Wait for myFunction to finish and print it's return value.
  std::cout << thr.get() << '\n';

  return 0;
}
```

#### Solutions

| Quiz | Solution                   |
| ---- | -------------------------- |
| 1    | [Show solution](quiz1.cpp) |
