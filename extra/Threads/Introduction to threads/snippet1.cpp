#include <algorithm>
#include <iostream>
#include <vector>

void getListsFromUser(std::vector<std::vector<std::string>> &vLists)
{
  std::size_t sLists{ 0 };

  std::cout << "How many lists would you like to enter?\n";

  std::cin >> sLists;

  // Resize the vector so it has enough space to hold all lists
  vLists.resize(sLists);

  for (std::size_t s{ 0 }; s < sLists; ++s)
  {
    std::size_t sNames{ 0 };

    std::cout << "How many names would you like to enter into list #" << (s + 1) << "?\n";

    std::cin >> sNames;

    // Get the next list
    auto &vActiveList{ vLists.at(s) };

    // Reserve space in the list to store the names
    vActiveList.resize(sNames);

    for (std::size_t s2{ 0 }; s2 < sNames; ++s2)
    {
      // Get a reference to the next name
      std::string &strName{ vActiveList.at(s2) };

      std::cout << "Enter name #" << (s2 + 1) << ": ";

      std::cin >> strName;
    }
  }
}

void sortLists(std::vector<std::vector<std::string>> &vLists)
{
  for (auto &vStrList : vLists)
  {
    std::sort(vStrList.begin(), vStrList.end());
  }
}

void printLists(std::vector<std::vector<std::string>> &vLists)
{
  for (const auto &vStrList : vLists)
  {
    for (const std::string &strName : vStrList)
    {
      std::cout << strName << ' ';
    }

    std::cout << '\n';
  }
}

int main(void)
{
  // Create a vector of vectors. The inner vectors are the name lists
  std::vector<std::vector<std::string>> vLists{ };

  getListsFromUser(vLists);

  sortLists(vLists);

  printLists(vLists);

  return 0;
}