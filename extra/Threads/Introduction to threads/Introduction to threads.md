## Required lessons
- [4.8 -- The auto keyword](https://www.learncpp.com/cpp-tutorial/4-8-the-auto-keyword/)
- [6.9a -- Dynamically allocating arrays](https://www.learncpp.com/cpp-tutorial/6-9a-dynamically-allocating-arrays/)
- [6.16 -- An introduction to std::vector](https://www.learncpp.com/cpp-tutorial/6-16-an-introduction-to-stdvector/)

---

### Recap

In quiz 1 of lesson [6.9a -- Dynamically allocating arrays](https://www.learncpp.com/cpp-tutorial/6-9a-dynamically-allocating-arrays/), you wrote a program that asked the user to enter a list of names, sorted the list using selection sort, and printed it.  
In this lesson we'll take a look at a similar program and, with the use of threads, speed up the program. Rather than asking the user for a single list of names we'll ask the user to enter one list after the other.

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

void getListsFromUser(std::vector<std::vector<std::string>> &vLists)
{
  std::size_t sLists{ 0 };

  std::cout << "How many lists would you like to enter?\n";

  std::cin >> sLists;

  // Resize the vector so it has enough space to hold all lists
  vLists.resize(sLists);

  for (std::size_t s{ 0 }; s < sLists; ++s)
  {
    std::size_t sNames{ 0 };

    std::cout << "How many names would you like to enter into list #" << (s + 1) << "?\n";

    std::cin >> sNames;

    // Get the next list
    auto &vActiveList{ vLists.at(s) };

    // Reserve space in the list to store the names
    vActiveList.resize(sNames);

    for (std::size_t s2{ 0 }; s2 < sNames; ++s2)
    {
      // Get a reference to the next name
      std::string &strName{ vActiveList.at(s2) };

      std::cout << "Enter name #" << (s2 + 1) << ": ";

      std::cin >> strName;
    }
  }
}

void sortLists(std::vector<std::vector<std::string>> &vLists)
{
  for (auto &vStrList : vLists)
  {
    std::sort(vStrList.begin(), vStrList.end());
  }
}

void printLists(std::vector<std::vector<std::string>> &vLists)
{
  for (const auto &vStrList : vLists)
  {
    for (const std::string &strName : vStrList)
    {
      std::cout << strName << ' ';
    }

    std::cout << '\n';
  }
}

int main(void)
{
  // Create a vector of vectors. The inner vectors are the name lists
  std::vector<std::vector<std::string>> vLists{ };

  getListsFromUser(vLists);

  sortLists(vLists);

  printLists(vLists);

  return 0;
}
```

`getListsFromUser` is asking the user how many lists they want to enter, followed by the user entering the size for each lists along with the elements.  
`sortLists` then loops through the lists, sorting one after the other.

Use of the program could look as follows
```
How many lists would you like to enter?
2
How many names would you like to enter into list #1?
3
Enter name #1: tom
Enter name #2: jim
Enter name #3: carl
How many names would you like to enter into list #2?
4
Enter name #1: maria
Enter name #2: alex
Enter name #3: stacy
Enter name #4: anton
carl jim tom
alex anton maria stacy
```

### What are threads?

As you might have discovered in previous lessons, sorting lists can be slow when there is lots of data. Let's say each list takes 1 second to sort and we're sorting 4 separate lists. This means that the sorting of each list will need to wait for the preceding lists to be sorted, even though the lists are unrelated. Resulting in an execution time of 4 seconds total.  
This is were threads come in handy. Threads allow you to make use of your processor's cores. Each core can run independently of the other cores, meaning that two tasks can be performed in parallel without affecting each other. By default, your program runs on a single core, which means that your code is executed in-order and there is only ever one point of active execution.  
By using threads we can start the sorting of all lists almost simultaneously to decrease the execution time from 4 seconds to 1 second, because now the lists don't have to wait for the others to finish.

### Basic use and problems

Before applying this idea to our sorting example, let's first look at how threads are used on a small scale.  
Some IDEs might do this automatically, but it some cases you will need to add the `-pthread` or `-lpthread` option to your compiler options in order to use threads.

```cpp
#include <iostream>
#include <thread> // For std::thread

void myFunction(void)
{
  std::cout << "myFunction is running\n";
}

int main(void)
{
  // Create a new thread object called "thr".
  // The argument to std::thread::thread is the function that's supposed to be
  // executed by the thread.
  // Threads start executing their function immediately after creation.
  std::thread thr{ myFunction };

  // myFunction is now running or has already finished

  std::cout << "Executing code in main\n";

  // If myFunction is still running, wait for it to finish before continuing to
  // execute code in main.
  thr.join();

  // At this point execution of myFunction has finished and thr is no longer
  // runnning.
  std::cout << "End of main\n";

  return 0;
}
```

Since execution of `myFunction` starts when `thr` is created and it only performs one operation you might expect "myFunction is running" to always be printed before "Executing code in main" is printed. But this is not the case.  
The code in `myFunction` is not atomic, ie. it consists of many small parts which can be split apart. This means that `myFunction` might be before, in, or after the process of printing the message to the screen when `main` continues to execute code. Possible output is
```
Executing code in main
myFunction is running
End of main
```
```
myFunction is running
Executing code in main
End of main
```

Note that both `main` and `myFunction` access the same output stream object (`std::cout`) at the same time. Accessing the same object from multiple places simultaneously can cause unexpected and undefined behavior.  
In the case of `std::cout <<`, it is guaranteed that the program will not crash, but the order in which text is inserted into the stream is undefined.  
Since we're using `std::cout <<` with only a single string as an argument we don't have to worry about the two messages getting mixed into each other.

The call to `thr.join` is required to prevent the destruction of `thr` when `main` exits. Omitting this call will cause your program to be forcefully terminated once `myFunction` finishes execution.  
There is also `thr.detach` which we could have used to keep on executing code asynchronously inside `main` without waiting for `myFunction` to finish. In that case
```
Executing code in main
End of main
myFunction is running
```
adds to the lists of possible outcomes.

To get a better understanding of the problem with concurrent access to the same object let's have a look at another example.

```cpp
#include <iostream>
#include <thread>

void myFunction(int iCycles)
{
  for (int i{ 0 }; i < iCycles; ++i)
  {
    std::cout << "Cycle " << (i + 1) << "/" << iCycles << '\n';
  }
}

int main(void)
{
  // We can pass arguments to threaded functions by appending them in the
  // constructor.
  std::thread thr1{ myFunction, 5 }; // Call myFunction(5)
  std::thread thr2{ myFunction, 7 }; // Call myFunction(7)

  // Wait for both threads to finish
  thr1.join();
  thr2.join();

  return 0;
}
```

We're now using multiple `<<` operators to print our message. The output may be split at any of those operators to insert a message from another thread. Possible output is
```
Cycle 1/Cycle 51/7
Cycle 2/7
Cycle 3/
Cycle 2/5
Cycle 3/5
Cycle 4/5
Cycle 5/5
7
Cycle 4/7
Cycle 5/7
Cycle 6/7
Cycle 7/7
```

For one, the loop iterations of both function calls are mixed. In this specific case, the first cycle is performed by the call to `myFunction(5)`, the second by `myFunction(7)`, followed by 4 cycles of `myFunction(5)`, then 4 more by `myFunction(7)`. The order will vary on every run of the program.  
More importantly, you can see that every call to `std::cout <<` can be followed by a call to `std::cout <<` from another thread. This is harmless for the program's stability but undesirable for an end-user.  
Other functions might not be adapted to be used in threads and can cause undefined behavior and crashes. For this reason you should make sure that the objects you're accessing from multiple threads are rated for this type of use,
for example by reading the ["Data races" section on cplusplus.com](http://www.cplusplus.com/reference/ostream/ostream/operator%3C%3C/#access).  
You'll later learn how to manually synchronize threads to avoid clashes.

Let's apply your new knowledge to the sorting example from the beginning.  
Because we want to allow each call to `std::sort` to run independent of the other, we need to execute them in a thread. The only catch being that `std::sort` is a template function.  
If you tried calling
```cpp
std::thread{ std::sort, vStrList.begin(), vStrList.end() };
```
you'll encounter a compilation error, because the compiler doesn't know which types of parameters `std::sort` will receive. When we called
```cpp
std::sort(vStrList.begin(), vStrList.end());
```
`std::sort` could automatically deduce it's parameter types from the given arguments ([Lesson 13.1 -- Function templates (On Alex' todo list)](https://www.learncpp.com/cpp-tutorial/131-function-templates/)). In order to make the code work we need to explicitly state the template types of `std::sort`.
```cpp
std::thread{ std::sort<std::vector<std::string>::iterator>, vStrList.begin(), vStrList.end() };
```
Putting this in action gives us
```cpp
void sortLists(std::vector<std::vector<std::string>> &vLists)
{
  for (auto &vStrList : vLists)
  {
    std::thread thr{ std::sort<std::vector<std::string>::iterator>, vStrList.begin(), vStrList.end() };

    // But we still need to call
    // thr.join();
    // or
    // thr.detach();
  }
}
```
If we called `thr.join` the loop wouldn't continue to it's next cycle before the current list has been sorted, bringing us back to what we started with.  
If we called `thr.detach` all threads would run at the same time, but `sortLists` wouldn't wait for the threads to finish before returning. When `main` calls `printLists` the lists may not yet be sorted.  
We could `detach` from the threads and use a counter to see how many threads are currently running and use a `while`-loop to wait until all threads are finished. This is known busy-waiting and it's bad, because your CPU is constantly working to check for a condition, resulting in high CPU usage for a very simple task.  
A better solution is to start all threads in the loop, adding the threads into a vector, and using a second loop which calls join on all threads.
```cpp
void sortLists(std::vector<std::vector<std::string>> &vLists)
{
  // Create a vector to hold our threads.
  std::vector<std::thread> vThr{ };

  // Reserve space in our vector to avoid unnecessary resizing
  vThr.reserve(vLists.size());

  for (auto &vStrList : vLists)
  {
    // Create a new thread inside vThr that calls
    // std::sort(vStrList.begin(), vStrList.end());
    vThr.emplace_back(std::sort<std::vector<std::string>::iterator>, vStrList.begin(), vStrList.end());
  }

  // At this point all threads are running or finished.
  // Join the threads so the sortLists function doesn't return before all the
  // threads are finished.
  for (std::thread &thr : vThr)
  {
    thr.join();
  }
}
```

### Putting everything together

[//]: # (snipppet4.cpp)

```cpp
#include <algorithm>
#include <iostream>
#include <thread>
#include <vector>

void getListsFromUser(std::vector<std::vector<std::string>> &vLists)
{
  std::size_t sLists{ 0 };

  std::cout << "How many lists would you like to enter?\n";

  std::cin >> sLists;

  vLists.resize(sLists);

  for (std::size_t s{ 0 }; s < sLists; ++s)
  {
    std::size_t sNames{ 0 };

    std::cout << "How many names would you like to enter into list #" << (s + 1) << "?\n";

    std::cin >> sNames;

    auto &vActiveList{ vLists.at(s) };

    vActiveList.resize(sNames);

    for (std::size_t s2{ 0 }; s2 < sNames; ++s2)
    {
      std::string &strName{ vActiveList.at(s2) };

      std::cout << "Enter name #" << (s2 + 1) << ": ";

      std::cin >> strName;
    }
  }
}

void sortLists(std::vector<std::vector<std::string>> &vLists)
{
  std::vector<std::thread> vThr{ };

  vThr.reserve(vLists.size());

  for (auto &vStrList : vLists)
  {
    vThr.emplace_back(std::sort<std::vector<std::string>::iterator>, vStrList.begin(), vStrList.end());
  }

  for (std::thread &thr : vThr)
  {
    thr.join();
  }
}

void printLists(std::vector<std::vector<std::string>> &vLists)
{
  for (const auto &vStrList : vLists)
  {
    for (const std::string &strName : vStrList)
    {
      std::cout << strName << ' ';
    }

    std::cout << '\n';
  }
}

int main(void)
{
  std::vector<std::vector<std::string>> vLists{ };

  getListsFromUser(vLists);

  sortLists(vLists);

  printLists(vLists);

  return 0;
}
```

The output is the same as before, but all lists are being sorted at the same time.  

### Common uses

Common uses for threads are video games, where one thread is responsible for rendering and another thread is doing all calculations, and everything that's using the network so your program doesn't block until a response to a request is received.