#include <iostream>
#include <thread> // For std::thread

void myFunction(void)
{
  std::cout << "myFunction is running\n";
}

int main(void)
{
  // Create a new thread object called "thr".
  // The argument to std::thread::thread is the function that's supposed to be
  // executed by the thread.
  // Threads start executing their function immediately after creation.
  std::thread thr{ myFunction };

  // myFunction is now running or has already finished

  std::cout << "Executing code in main\n";

  // If myFunction is still running, wait for it to finish before continuing to
  // execute code in main.
  thr.join();

  // At this point execution of myFunction has finished and thr is no longer
  // runnning.
  std::cout << "End of main\n";

  return 0;
}