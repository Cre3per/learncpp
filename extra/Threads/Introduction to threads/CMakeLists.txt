cmake_minimum_required(VERSION 3.0.0)
project(introduction_to_threads VERSION 1.0.0)

set(CMAKE_PROJECT_NAME ${PROJECT_NAME})
set(CMAKE_PROJECT_VERSION ${PROJECT_VERSION})

set(CMAKE_CXX_COMPILER "clang++")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m64 -Wall -Wextra -pedantic-errors -Wfatal-errors -std=c++2a")

find_package("Threads")

add_executable("snippet1"
  "snippet1.cpp")

  add_executable("snippet2"
    "snippet2.cpp")
target_link_libraries("snippet2" "${CMAKE_THREAD_LIBS_INIT}")

add_executable("snippet3"
    "snippet3.cpp")
target_link_libraries("snippet3" "${CMAKE_THREAD_LIBS_INIT}")

add_executable("snippet4"
    "snippet4.cpp")
target_link_libraries("snippet4" "${CMAKE_THREAD_LIBS_INIT}")