#include <algorithm>
#include <iostream>
#include <thread>
#include <vector>

void getListsFromUser(std::vector<std::vector<std::string>> &vLists)
{
  std::size_t sLists{ 0 };

  std::cout << "How many lists would you like to enter?\n";

  std::cin >> sLists;

  vLists.resize(sLists);

  for (std::size_t s{ 0 }; s < sLists; ++s)
  {
    std::size_t sNames{ 0 };

    std::cout << "How many names would you like to enter into list #" << (s + 1) << "?\n";

    std::cin >> sNames;

    auto &vActiveList{ vLists.at(s) };

    vActiveList.resize(sNames);

    for (std::size_t s2{ 0 }; s2 < sNames; ++s2)
    {
      std::string &strName{ vActiveList.at(s2) };

      std::cout << "Enter name #" << (s2 + 1) << ": ";

      std::cin >> strName;
    }
  }
}

void sortLists(std::vector<std::vector<std::string>> &vLists)
{
  std::vector<std::thread> vThr{ };

  vThr.reserve(vLists.size());

  for (auto &vStrList : vLists)
  {
    vThr.emplace_back(std::sort<std::vector<std::string>::iterator>, vStrList.begin(), vStrList.end());
  }

  for (std::thread &thr : vThr)
  {
    thr.join();
  }
}

void printLists(std::vector<std::vector<std::string>> &vLists)
{
  for (const auto &vStrList : vLists)
  {
    for (const std::string &strName : vStrList)
    {
      std::cout << strName << ' ';
    }

    std::cout << '\n';
  }
}

int main(void)
{
  std::vector<std::vector<std::string>> vLists{ };

  getListsFromUser(vLists);

  sortLists(vLists);

  printLists(vLists);

  return 0;
}