#include <iostream>
#include <thread>

void myFunction(int iCycles)
{
  for (int i{ 0 }; i < iCycles; ++i)
  {
    std::cout << "Cycle " << (i + 1) << "/" << iCycles << '\n';
  }
}

int main(void)
{
  // We can pass arguments to threaded functions by appending them in the
  // constructor.
  std::thread thr1{ myFunction, 5 }; // Call myFunction(5)
  std::thread thr2{ myFunction, 7 }; // Call myFunction(7)

  // Wait for both threads to finish
  thr1.join();
  thr2.join();

  return 0;
}