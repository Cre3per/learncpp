# Execution Policies

Many times we have tasks that could be parallelized, ie. we want to call the
same function many times with different arguments.  
Recall the creature quiz from chapter 11. It had a creature `class` like this.

```cpp
class Creature
{
// ...

public:

  void reduceHealth(int health);

// ...
};
```

Suppose we added groups of monsters that we stored in an
`std::vector<Creature> vGroup`. These groups can be large, up to
10'000'000 monsters
per group. The player throws a damaging potion into the group, hitting all
monsters. Now we need to decrease each `Creature`'s health by some amount.

```cpp
for (Creature &monster : vGroup)
{
  monster.reduceHealth(3);
}
```

This will work, but given the size of `vGroup`, it takes very long. Each monster
has to wait until all monsters before it had their health decreased. Like
standing in line at an amusement park. This is called _sequential_ processing,
because the list is processed in sequence. Amusement parks don't just have one
cash register, there are many. Just like the cores in your CPU. By using the
`for`-loop above, only a single core is used while the others idle.  
We could separate the task into several `std::thread`s and let each thread work
on a subset of our data. This requires manual segmentation and management of the
threads. That's too much effort and is way less readable than the loop we just
saw.  
Luckily, support for parallelism is built into C++. As mentioned in
[lesson 6.12a](../6/12a.md), `std::for_each` has execution policies. A lot of
the standard algorithms have execution policies. Execution policies decide how
your code should be sequenced or parallelized. There are 4 standard execution
policies

#### `std::execution::sequence_policy` or `std::execution::seq`
Your data is processed in sequence. This is the same as the `for`-loop we used
earlier. `std::execution::seq` can be used to enable sequential execution for
multiple function through a type alias.

#### `std::execution::unsequenced`
Your data is processed in one thread, but several operations that share the
same operator might be merged into a single operation. In the above example, if
your compiler and CPU can handle it, several monsters could get their health
decreased at the same time, even though only one thread is used. This is called
_vectorization_, it's supported by most modern CPUs support.

#### `std::execution::parallel_policy` or `std::execution::par`
Your data can be processed in parallel by multiple threads. Inside each thread,
the data is processed in sequence.

#### `std::execution::parallel_unsequenced::policy` or `std::execution::par_unseq`
Your data can be processed in parallel by multiple threads. Inside each thread,
the data can be vectorized, see `std::execution::unsequenced`.

To use an execution policy, you simply pass it to the algorithm you're using as
the first argument.

<!--
TODO:
Why would we use std::execution::par or std::execution::unseq?
Which data is unsequencable but not parallelizable or the other way around?
-->

<!--
TODO:
ranges?
-->

```cpp
#include <execution> // For std::execution

// ...

// Reduce the health parallel and vectorized.
std::for_each(std::execution::par_unseq,
  vGroup.begin(), vGroup.end(), [](Creature& monster)
  {
    monster.reduceHealth(3);
  });

// All monsters had their health reduced. All threads that were created by
// std::for_each have finished.

```

That's all you have to do. The compiler manages all the segmentation, threading,
and vectorization for you.
```
Note: Your compiler might use certain libraries for parallelism. If your program
doesn't link, search for the error message and you'll find which libraries you
need to add.
```

## Pitfalls

### Thread safety
Although execution policies do a lot of the work for you, it's up to you to
choose the right execution policy. You have to make sure that the functions and
data you're accessing are thread-safe.  

```cpp
std::vector<std::string> vStrNames{
  "Myla",
  "Luna",
  "Laurent",
  "Max",
  "Deanna"
};

std::vector<int> vINameLengths{};

// Fill vINameLengths with the length of each name.
std::for_each(std::execution::parallel_unseq,
  vStrNames.begin(), vStrNames.end(), [](const std::string &strName)
  {
     vINameLengths.push_back(static_cast<int>(strName.length()));
  });
```

`vINameLengths` has to reallocate its data and increase its internal size
counter when we push an element. Neither of these operations can be
parallelized. Doing so anyway causes a race condition.  
We can prepare the vector so that neither a reallocation nor resizing is
necessary.

```cpp
// Default construct as many ints as we need.
std::vector<int> vINameLengths(vStrNames.size());

// Use std::transform to get an output iterator
std::transform(std::execution::parallel_unseq,
  vStrNames.begin(), vStrNames.end(),
  vINameLengths.begin(), // Output to vINameLengths
  [](const std::string &strName)
  {
     return static_cast<int>(strName.length());
  });
```

#### Trick question
Assume you have multiple wallets and you want to know how many coins you have in
total.

```cpp
std::vector<int> vWallets{ 1000, 40000, 3, 92, 8992 };
int iSum{ 0 };

std::for_each(/**/, vWallets.begin(), vWallets.end(), [](int iCoins)
{
  iSum += iCoins;
});
```

Which execution should be used?

The answer is that you shouldn't use `std::for_each` at all in this case. If you
wanted to use `std::for_each`, you'd have to use none or a sequential execution
policy, because `iSum` cannot be written to in parallel. Instead, you should use
`std::reduce`. `std::reduce` is superior to `std::accumulate` is the aspect of
parallelism, because it may group the data. `std::accumulate` is always
sequential.

### Overhead

Calling a function with a parallel or vectorized execution policy isn't free.
If you're dealing with small amounts of data, the overhead caused by
parallelizing is probably greater than the performance benefit.

```cpp
#include <algorithm>
#include <chrono>
#include <execution>
#include <iostream>
#include <numeric>
#include <vector>

/**
 * Measures 
 */
void measure(std::function<void(void)> fn)
{
  auto tpStart{ std::chrono::high_resolution_clock::now() };

  fn();

  auto tpEnd{ std::chrono::high_resolution_clock::now() };

  auto durTimeElapsed{ tpEnd - tpStart };

  std::cout
      << std::chrono::duration_cast<std::chrono::microseconds>(durTimeElapsed).count()
      << "us\n";
}

void increment(int& i)
{
  ++i;
}

int main(void)
{
  // Create an std::vector and fill it with 10 consecutive numbers.
  std::vector<int> vI(10'000);
  std::iota(vI.begin(), vI.end(), 0);

  measure([&]() {
    std::for_each(std::execution::seq, vI.begin(), vI.end(), increment);
  });

  measure([&]() {
    std::for_each(std::execution::unseq, vI.begin(), vI.end(), increment);
  });

  measure([&]() {
    std::for_each(std::execution::par, vI.begin(), vI.end(), increment);
  });

  measure([&]() {
    std::for_each(std::execution::par_unseq, vI.begin(), vI.end(), increment);
  });

  return 0;
}
```

On the author's machine, the above code produced the following results for
10, 10'000 and 10'000'000 elements ("us" means microseconds, "ms" milliseconds).

| Execution policy            | 10    | 10'000 | 10'000'000 |
| --------------------------- | ----- | ------ | ---------- |
| `std::execution::seq`       | 1us   | 403us  | 134ms      |
| `std::execution::unseq`     | 1us   | 218us  | 101ms      |
| `std::execution::par`       | 839us | 1786us | 39ms       |
| `std::execution::par_unseq` | 10us  | 168us  | 21ms       |

Implementations are allowed to ignore a requested execution policy. Some might
have better better support for certain execution policies than others.

Using a parallel execution policy for very small sets is 10 to 800 times slower
than processing the data in sequence.  
If your operation can be parallelized, it makes sense to use
`std::execution::unseq` for sets up to around 10'000 elements, from there on use
`std::execution::par_unseq`.