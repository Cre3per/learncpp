`iRemainingPlayers` is captured by reference, but it will die at the end of
`registerCallbacks`. By the time it is used in the lambda, it is a dangling
reference.